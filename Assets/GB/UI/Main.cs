using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GB;
using GB.UI;
using UnityEngine.UI;

public class Main : UIScreen
{

    ProcessMain data;


    private void Awake()
    {
        data = new ProcessMain();
        RegistButton();
    }

    private void OnEnable()
    {
        Presenter.Bind("Main",this);
    }

    private void OnDisable() 
    {
        Presenter.UnBind("Main", this);

    }

    public void RegistButton()
    {
        foreach(var v in mButtons)
            v.Value.onClick.AddListener(() => { OnButtonClick(v.Key);});
        
    }

    public void OnButtonClick(string key)
    {
        switch(key)
        {
            case "Btn_Players" :
                data.BtnOpenClick();
                //Presenter.Notify("")
            break;
        }
    }

 public override void OnChangeValue(string key, string value)
    {
        switch (key)
        {
       
        }
    }

 public override void OnChangeValue(string key, Mail data) 
    {
        switch (key)
        {
           //ex : Message<int> data = (Message<int>)data;
       
        }


    }

}