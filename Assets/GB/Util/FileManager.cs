using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Text;
using System;
using System.Threading.Tasks;
public class FileManager : MonoBehaviour
{
    static FileManager _Instance;
    public static FileManager Instance
    {
        get
        {
            if (_Instance == null)
                _Instance = FindObjectOfType<FileManager>();
            if (_Instance == null)
                _Instance = new GameObject("FileManager").AddComponent<FileManager>();

            return _Instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    Dictionary<string, object> _Datas = new Dictionary<string, object>();

    public static void Set(string key, object data)
    {

        Instance._Datas[key] = data;
    }

    /// <summary>
    /// 데이터가 있는지 체크
    /// </summary>
    /// <param name="key">키 입력</param>
    /// <returns></returns>
    public static bool ContainsKey(string key)
    {
        return Instance._Datas.ContainsKey(key);
    }

    /// <summary>
    ///  데이터 가져오기
    /// </summary>
    /// <param name="key">키 입력</param>
    /// <typeparam name="T">데이터 타입</typeparam>
    /// <returns></returns>
    public static T Get<T>(string key)
    {
        return (T)Convert.ChangeType(Instance._Datas[key], typeof(T), System.Globalization.CultureInfo.InvariantCulture);
    }

    /// <summary>
    /// 데이터 저장
    /// </summary>
    public static void SaveAsync()
    {
        Task.Run(() => Instance.AsncSave());
    }

    public static void Save()
    {
        string encryptData = Encrypt(ToJson());

        string path = Application.dataPath;
        path = string.Format(@"{0}/SaveData.bytes", path);

        System.IO.File.WriteAllText(path, encryptData);
    }

    async void AsncSave()
    {
        await SaveAsyncTask();
    }

    async Task SaveAsyncTask()
    {

        await Task.Run(() =>
        {
            string encryptData = Encrypt(ToJson());

            string path = Application.dataPath;
            path = string.Format(@"{0}/SaveData.bytes", path);

            System.IO.File.WriteAllText(path, encryptData);

        });
    }

    /// <summary>
    /// 파일 로드
    /// </summary>
    public static void LoadAsync()
    {
        Task.Run(() => Instance.AsncLoad());
    }
    public static void Load()
    {
        try
        {
            string path = Application.dataPath;
            path = string.Format(@"{0}/SaveData.bytes", path);
            string encryptData = System.IO.File.ReadAllText(path);
            string json = Decrypt(encryptData);
            Instance._Datas = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        }
        catch
        {
            Debug.LogWarning("None File");
        }

    }

    async void AsncLoad()
    {
        await LoadAsyncTask();
    }

    async Task LoadAsyncTask()
    {

        await Task.Run(() =>
        {
            try
            {
                string path = Application.dataPath;
                path = string.Format(@"{0}/SaveData.bytes", path);
                string encryptData = System.IO.File.ReadAllText(path);
                string json = Decrypt(encryptData);
                Instance._Datas = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            }
            catch
            {
                Debug.LogWarning("None File");
            }
        });
    }





    /// <summary>
    /// JSON 
    /// </summary>
    /// <returns></returns>
    public static string ToJson()
    {
        return JsonConvert.SerializeObject(Instance._Datas);
    }

    /// <summary>
    /// 모든 데이터 정리
    /// </summary>
    public static void Clear()
    {
        Instance._Datas.Clear();
    }

    /// <summary>
    /// 암호화
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string Encrypt(string data)
    {
        var bytes = System.Text.Encoding.UTF8.GetBytes(data);
        var encrypt = Convert.ToBase64String(bytes);
        return encrypt;
    }


    /// <summary>
    /// 복호화
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string Decrypt(string data)
    {
        var bytes = Convert.FromBase64String(data);
        var decrypt = Encoding.UTF8.GetString(bytes);

        return decrypt;
    }

}
